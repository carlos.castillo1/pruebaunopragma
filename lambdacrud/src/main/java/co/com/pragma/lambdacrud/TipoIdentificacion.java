package co.com.pragma.lambdacrud;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "tipo_identificacion")
public class TipoIdentificacion {
	
	@DynamoDBHashKey
	Integer id;
	@DynamoDBAttribute
	String nombre;
	
	public TipoIdentificacion() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TipoIdentificacion(Integer id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	
}
