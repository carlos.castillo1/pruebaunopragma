package co.com.pragma.lambdacrud;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class CrudClient implements RequestHandler<Request, Object> {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("alv");
	}

	@Override
	public Object handleRequest(Request request, Context context) {
		try {
			AmazonDynamoDB db = AmazonDynamoDBClientBuilder.defaultClient();
	        DynamoDBMapper mapper = new DynamoDBMapper(db);
	        Cliente cliente = null;
	        TipoIdentificacion tipoIdentificacion = null;
	        
	        switch (request.getMethod()) {
	            
	        /*case "GET":
        	List<TipoIdentificacion> listTipoIdentificacion = new ArrayList<>();
        	listTipoIdentificacion = mapper.scan(TipoIdentificacion.class, new DynamoDBScanExpression());
	        if(listTipoIdentificacion.isEmpty()) {
	        	return "lista tipo de identificacion vacia";
	        }
	        return listTipoIdentificacion ;
        	
        case "POST":	
			tipoIdentificacion = request.getTipoIdentificacion();
			mapper.save(tipoIdentificacion);
			return tipoIdentificacion;
		case "PUT":
			tipoIdentificacion = mapper.load(TipoIdentificacion.class, request.getTipoIdentificacion().getId());
			if(tipoIdentificacion == null) {
				return "el tipo de identificacion no existe";
			}
			mapper.save(tipoIdentificacion);
			return tipoIdentificacion;
		case "DELETE":
			tipoIdentificacion = mapper.load(TipoIdentificacion.class, request.getId());
			if(tipoIdentificacion == null ) {
				return "el tipo de identificacion no existe";
			}
			mapper.delete(tipoIdentificacion);
			return "tipo de identificacion eliminado : " + tipoIdentificacion.getId();*/

	        
	        
		    case "GET":        
		        
		        if (request.getId() == null || request.getId() == 0) {
		        	List<Cliente> listaClientes = new ArrayList<>();
			        listaClientes = mapper.scan(Cliente.class, new DynamoDBScanExpression());
			        if(listaClientes.isEmpty()) {
			        	return "lista de clientes vacia";
			        }
			        return listaClientes ;
				} else {
					cliente = mapper.load(Cliente.class, request.getId());
					return cliente;
				}
			case "POST":	
				Cliente cliAux = cliente = mapper.load(Cliente.class, request.getCliente().getId());
				if(cliAux == null) {
					cliente = request.getCliente();
					mapper.save(cliente);
					return cliente;
				}
				return "el id del cliente ya esta registrado";

			case "PUT":
				cliente = mapper.load(Cliente.class, request.getCliente().getId());

				tipoIdentificacion = mapper.load(TipoIdentificacion.class, cliente.getTipoIdentificacion());

				if (cliente == null) {
					return "el cliente no existe";
				}
				if(tipoIdentificacion == null) {
					return "EL tipo de identificacion es incorrecto";
				}
				mapper.save(request.getCliente());
				return request.getCliente();
			case "DELETE":
				cliente = mapper.load(Cliente.class, request.getId());
				if (cliente == null) {
					return "el cliente no existe";
				}
				mapper.delete(cliente);
				return "cliente eliminado : " + cliente.getId();
			default:
				return "especifique un metodo";
			}
		} catch (Exception e) {
			System.out.println("error : " + e.getMessage());
			return "causa : " + e.getCause() + "\nerror : " + e.getMessage();
		}

	}

}
