package co.com.pragma.lambdacrud;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "cliente")
public class Cliente {
	
	@DynamoDBHashKey
	Integer id;
	@DynamoDBAttribute
	String nombre;
	@DynamoDBAttribute
	String apellido;
	@DynamoDBAttribute
	Integer tipoIdentificacion;
	@DynamoDBAttribute
	Integer noIdentificacion;
	@DynamoDBAttribute
	Integer edad;
	@DynamoDBAttribute
	String ciudadNacimiento;

	public Cliente() {
		super();
	}

	public Cliente(Integer id, String nombre, String apellido, Integer tipoIdentificacion,
			Integer noIdentificacion, Integer edad, String ciudadNacimiento) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.tipoIdentificacion = tipoIdentificacion;
		this.noIdentificacion = noIdentificacion;
		this.edad = edad;
		this.ciudadNacimiento = ciudadNacimiento;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Integer getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(Integer tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public Integer getNoIdentificacion() {
		return noIdentificacion;
	}

	public void setNoIdentificacion(Integer noIdentificacion) {
		this.noIdentificacion = noIdentificacion;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public String getCiudadNacimiento() {
		return ciudadNacimiento;
	}

	public void setCiudadNacimiento(String ciudadNacimiento) {
		this.ciudadNacimiento = ciudadNacimiento;
	}
	
	
}
