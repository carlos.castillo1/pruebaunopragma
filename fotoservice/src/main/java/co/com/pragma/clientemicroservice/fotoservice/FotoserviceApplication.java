package co.com.pragma.clientemicroservice.fotoservice;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Import;

import springfox.documentation.swagger2.configuration.Swagger2DocumentationConfiguration;

@SpringBootApplication
@EnableDiscoveryClient
@EnableEurekaClient
@Import(Swagger2DocumentationConfiguration.class)
public class FotoserviceApplication implements CommandLineRunner {
	

	public static void main(String[] args) {
		SpringApplication.run(FotoserviceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
