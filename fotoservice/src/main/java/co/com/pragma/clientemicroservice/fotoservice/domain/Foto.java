package co.com.pragma.clientemicroservice.fotoservice.domain;

import javax.validation.constraints.NotNull;

import org.springframework.context.annotation.Bean;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Document(collection = "foto")
@Data @NoArgsConstructor @AllArgsConstructor

public class Foto {
	
	@Id
	@NotNull
	private String id;
	
	private String foto;
	
	private Integer idUsr;
}
