package co.com.pragma.clientemicroservice.fotoservice.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import co.com.pragma.clientemicroservice.fotoservice.domain.Foto;
import co.com.pragma.clientemicroservice.fotoservice.dto.FotoDto;

@Mapper(componentModel = "spring")
public interface FotoMapper {
	
	public FotoDto FotoToFotoDTO(Foto Foto);
	public Foto FotoDTOToFoto(FotoDto FotoDTO);
	
	public List<FotoDto> listFotoToListFotoDTO(List<Foto> listFoto);
	public List<Foto> listFotoDTOToListFoto(List<FotoDto> listFotoDTO);
}
