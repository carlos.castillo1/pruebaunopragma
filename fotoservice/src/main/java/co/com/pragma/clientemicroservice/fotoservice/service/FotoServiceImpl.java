package co.com.pragma.clientemicroservice.fotoservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import co.com.pragma.clientemicroservice.fotoservice.domain.Foto;
import co.com.pragma.clientemicroservice.fotoservice.dto.FotoDto;
import co.com.pragma.clientemicroservice.fotoservice.mapper.FotoMapper;
import co.com.pragma.clientemicroservice.fotoservice.repository.FotoRepository;

@Service
public class FotoServiceImpl implements FotoService {

	@Autowired
	private FotoRepository fotoRepository;
	
	@Autowired
	private FotoMapper fotoMapper;
	
	@Override
	public ResponseEntity<?> consultarFotos() throws Exception {
		
		List<Foto> listaFoto = this.fotoRepository.findAll();
		
		if(listaFoto.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK).body("No hay fotos guardadas");
		}else {
			return ResponseEntity.status(HttpStatus.OK).body(this.fotoMapper.listFotoToListFotoDTO(listaFoto));
		}
	}

	@Override
	public ResponseEntity<?> consultarFotoPorId(String idFoto) throws Exception {
		
		Optional<Foto> foto = this.fotoRepository.findById(idFoto);
		
		if(foto.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("id incorrecto");
		}else{
			return ResponseEntity.ok().body(this.fotoMapper.FotoToFotoDTO(foto.get()));
		}
	}

	@Override
	public ResponseEntity<?> guardarFoto(FotoDto nuevoFoto) throws Exception {
		
		if(nuevoFoto.getFoto().equals("") || nuevoFoto.getFoto().equals(null)) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("el argumento foto esta vacio");
		}if(nuevoFoto.getIdUsr() == null || nuevoFoto.getIdUsr().equals("")) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("No hay un id de usuario");
		}
		Foto foto = this.fotoMapper.FotoDTOToFoto(nuevoFoto);
		return ResponseEntity.status(HttpStatus.CREATED).body(this.fotoMapper.FotoToFotoDTO(this.fotoRepository.save(foto)));	
	}

	@Override
	public ResponseEntity<?> eliminarFotoPorId(String idFoto) throws Exception {
		Optional<Foto> foto = this.fotoRepository.findById(idFoto);
		
		if(foto.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("id incorrecto");
		}else{
			this.fotoRepository.deleteById(idFoto);
			return ResponseEntity.status(HttpStatus.OK).body("");
		}
		
	}

	@Override
	public ResponseEntity<?> actualizarFoto(FotoDto newFoto) throws Exception {
		
		Optional<Foto> foto = this.fotoRepository.findById(newFoto.getId());
		
		if(foto.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("El id de la foto no existe");
		}else {
			if(newFoto.getFoto() == null || newFoto.getFoto().equals("")) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("no hay foto que guardar");
			}else {
				return ResponseEntity.status(HttpStatus.CREATED).body(this.fotoMapper.FotoToFotoDTO(this.fotoRepository.save(this.fotoMapper.FotoDTOToFoto(newFoto))));
			}
		}
	}

	@Override
	public ResponseEntity<?> consultarFotoPorIdUsr(Integer id) throws Exception {
		
		Foto foto = this.fotoRepository.findByIdUsr(id);
		
		if(foto == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("no hay fotos para el id de usuario : " + id);
		}else {
			return ResponseEntity.status(HttpStatus.CREATED).body(this.fotoMapper.FotoToFotoDTO(foto));
		}
	}

}
