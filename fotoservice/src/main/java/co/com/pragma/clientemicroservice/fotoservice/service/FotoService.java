package co.com.pragma.clientemicroservice.fotoservice.service;

import org.springframework.http.ResponseEntity;

import co.com.pragma.clientemicroservice.fotoservice.dto.FotoDto;


public interface FotoService {
	
	public ResponseEntity<?> consultarFotos() throws Exception;
	
	public ResponseEntity<?> consultarFotoPorId(String idFoto) throws Exception;
	
	public ResponseEntity<?> consultarFotoPorIdUsr(Integer id) throws Exception;
	
	public ResponseEntity<?> guardarFoto(FotoDto nuevoFoto)throws Exception;
	
	public ResponseEntity<?> eliminarFotoPorId(String idFoto)throws Exception;

	public ResponseEntity<?> actualizarFoto(FotoDto Foto)throws Exception;

}
