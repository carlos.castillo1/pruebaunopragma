package co.com.pragma.clientemicroservice.fotoservice.repository;

import java.io.Serializable;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import co.com.pragma.clientemicroservice.fotoservice.domain.Foto;

@Repository
public interface FotoRepository extends MongoRepository<Foto, Serializable> {

	public Foto findByIdUsr(Integer idUsr)throws Exception;
}
