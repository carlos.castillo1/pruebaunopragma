package co.com.pragma.clientemicroservice.fotoservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.pragma.clientemicroservice.fotoservice.dto.FotoDto;
import co.com.pragma.clientemicroservice.fotoservice.service.FotoService;


@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api/foto")
public class FotoController {
	
	@Autowired
	private FotoService fotoService;
	
	
	@GetMapping("/")
	public ResponseEntity<?> consultarTfotos(){
		try {
			return fotoService.consultarFotos();
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@PostMapping("/")
	public ResponseEntity<?> guardarFoto(@RequestBody FotoDto foto){
		try {
			
			return fotoService.guardarFoto(foto);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	} 
	
	@GetMapping("/{id}")
	public ResponseEntity<?> consultarFotoPorId(@PathVariable("id") String id){
		
		try {
			return fotoService.consultarFotoPorId(id);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@GetMapping("/cliente/{id}")
	public ResponseEntity<?> consultarFotoPorIdUsr(@PathVariable("id") Integer id){
		
		try {
			return fotoService.consultarFotoPorIdUsr(id);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@PutMapping("/")
	public ResponseEntity<?> actualizarFotoPorId(@RequestBody FotoDto foto){
		try {
			
			return this.fotoService.actualizarFoto(foto);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarFoto(@PathVariable("id") String id){
		try {
			
			return this.fotoService.eliminarFotoPorId(id);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

}
