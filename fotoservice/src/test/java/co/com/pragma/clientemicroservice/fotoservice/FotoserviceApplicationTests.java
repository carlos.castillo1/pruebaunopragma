package co.com.pragma.clientemicroservice.fotoservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import co.com.pragma.clientemicroservice.fotoservice.repository.FotoRepository;
import co.com.pragma.clientemicroservice.fotoservice.service.FotoService;

@SpringBootTest
class FotoserviceApplicationTests {
	
	@Autowired
	private FotoService fotoService;
	
	@Autowired
	private FotoRepository fotoRepository;
	
	private final static Logger log = LoggerFactory.getLogger(FotoserviceApplicationTests.class);

	/*@Test
	void pruebaConsultaFoto() throws Exception {
		
		String id = "60d03bbad98cf226c3706ad3";
		
		FotoDto foto = this.fotoService.consultarFotoPorId(id);
		
		assertEquals(foto.getId(), id);
	}*/
	
	/*@Test
	void pruebaConsultaFotoPorIdUsuario() throws Exception {
		
		Integer noId = 7;
		FotoDto foto = this.fotoService.consultarFotoPorIdUsr(noId);
		assertEquals(foto.getIdUsr(), noId);
	}*/
	
	/*@Test
	void probarIdUsrDespuesDeGuardarUnaFoto() throws Exception {
		
		FotoDto foto = FotoDto.builder().foto("URL FOTO TEST")
				.idUsr(98).build();
		
		this.fotoService.guardarFoto(foto);
		FotoDto fotoDos = this.fotoService.consultarFotoPorIdUsr(98);
		
		assertEquals(98, fotoDos.getIdUsr());
	}*/
	
	/*@Test
	void eliminarFoto() throws Exception {
		
		Foto foto = fotoRepository.findByIdUsr(98);
		
		this.fotoService.eliminarFotoPorId(foto.getId());
		
		Optional<Foto> fotoAux = fotoRepository.findById(foto.getId());
		
		log.info("id usuario : " + fotoAux.get().getIdUsr());
		
		assertNotEquals(98, fotoAux.get().getIdUsr() );
	}*/

}
