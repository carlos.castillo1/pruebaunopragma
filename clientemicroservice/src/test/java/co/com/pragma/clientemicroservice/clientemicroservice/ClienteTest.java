package co.com.pragma.clientemicroservice.clientemicroservice;

import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Date;
import java.util.Optional;
import org.hibernate.HibernateException;
import org.hibernate.LazyInitializationException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import co.com.pragma.clientemicroservice.clientemicroservice.domain.Cliente;
import co.com.pragma.clientemicroservice.clientemicroservice.domain.TipoIdentificacion;
import co.com.pragma.clientemicroservice.clientemicroservice.dto.ClienteDto;
import co.com.pragma.clientemicroservice.clientemicroservice.dto.TipoIdentificacionDto;
import co.com.pragma.clientemicroservice.clientemicroservice.mapper.ClienteMapper;
import co.com.pragma.clientemicroservice.clientemicroservice.repository.ClienteRepository;
import co.com.pragma.clientemicroservice.clientemicroservice.service.ClienteService;
import co.com.pragma.clientemicroservice.clientemicroservice.service.TipoIdentificacionService;
import net.bytebuddy.asm.Advice.Thrown;


@SpringBootTest
public class ClienteTest {


	private ClienteService clienteServiceMock = Mockito.mock(ClienteService.class);
	
	

	@Autowired
	private TipoIdentificacionService tipoIdentificacionService;
	

	private ClienteRepository clienteRepository  = Mockito.mock(ClienteRepository.class);
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private ClienteMapper clienteMapper;

	private final static Logger log = LoggerFactory.getLogger(ClienteTest.class);

	
	@BeforeEach
	void setUp() throws Exception {
		
		ClienteDto clienteMock = ClienteDto.builder().id(7).nombre("alfredo").apellido("ramirez")
				.ciudadNacimiento("cali").edad(23).idTi(1).noIdentificacion(65846851).build();
		
		Cliente clienteMockDos = new Cliente(7, "alfredo", "ramirez test", 23, "cali", 65846851, new TipoIdentificacion(1,"cedula",null));
		
		//Mockito.when(clienteServiceMock.consultarClientePorId(7)).thenReturn(clienteMock);

		clienteMock.setNoIdentificacion(65846851);
		//Mockito.when(clienteServiceMock.consultarClientePorIdentificacion(65846851)).thenReturn(clienteMock);

		//Mockito.when(clienteRepository.findById(clienteMock.getId())).thenReturn( ) );
		
	}
	
	
	
	/*
	 * prueba si un cliente es el que se pide
	
	@Test
	void consultarCLientePorId() {
		// Arrange
		Integer id = 7;
		ClienteDto cli = null;
		try {
			// Act
			cli = this.clienteServiceMock.consultarClientePorId(id);
			// Assert
			assertEquals(cli.getId(), id);
		} catch (Exception e) {
			log.info(e.getMessage());
			fail("no se consulto el cliente o no son iguales, info : " + e.getMessage());
		}
	} */

	/*
	 * prueba si un cliente es el que se pide segun el numero de identificacion
	 
	@Test
	void consultarClientePorIdentificacion() {
		// Arrange
		Integer identificacion = 65846851;
		ClienteDto cli = null;
		try {
			// Act
			cli = this.clienteServiceMock.consultarClientePorIdentificacion(identificacion);
			// Assert
			assertEquals(cli.getNoIdentificacion(), identificacion);
		} catch (Exception e) {
			log.info(e.getMessage());
			fail("no se consulto el cliente o no son iguales, info : " + e.getMessage());
		}
	}*/

	/*
	 * pueba se si guardo un cliente
	 
	@Test
	void guardarCliente() {

		try {
			// Arrange
			
			TipoIdentificacionDto ti = this.tipoIdentificacionService.consultarTipoIdentificacionPorId(1);

			ClienteDto cli = ClienteDto.builder().noIdentificacion(65846851).nombre("seed").apellido("test").edad(25)
					.foto("foto test url").ciudadNacimiento("cali").noIdentificacion(435465).idTi(ti.getId()).build();
			// Act
			this.clienteService.guardarCliente(cli);
			ClienteDto newCli = this.clienteService.consultarClientePorIdentificacion(cli.getNoIdentificacion());
			// Assert
			assertEquals(newCli.getNoIdentificacion(), cli.getNoIdentificacion());
			
		} catch (Exception e) {
			log.info(e.getMessage());
			fail("el cliente no se pudo guardar, info : " + e.getMessage());
		}

	}*/
	
	/*
	 * prueba si se pudo eliminar un cliente
	 * 
	 

	@Test
	void eliminarCliente() {
		// Arrange
		Integer id = 4564365;
		ClienteDto cli = null;
		// Act
		try {
			this.clienteService.eliminarClientePorId(id);
			cli = this.clienteService.consultarClientePorId(id);
			// Assert
			assertEquals(cli, null);
		} catch (Exception e) {
			log.info("no se elimino el cliente, info : " + e.getMessage());
			fail("no se elimino el cliente, info : " + e.getMessage());
		}
	}*/
	
	@Test
	@Transactional
	void actualizarCliente() {
		Integer idCliente = 7;
		Cliente cliDos = null;
		ClienteDto cliUno = null;
	    
		try {
			cliUno = ClienteDto.builder().id(7).nombre("alfredo").apellido("ramirez test")
					.ciudadNacimiento("cali").edad(23).idTi(1).noIdentificacion(65846851).build();
			log.info("apellido uno : " + cliUno.getApellido() + "  id : " + cliUno.getId());
			this.clienteService.actualizarCliente(cliUno);
			cliDos = this.clienteRepository.findById(idCliente).get();
			
			assertEquals(cliUno.getApellido(), cliDos.getApellido());
			
		} catch (Exception e) {
			log.info("no se actualizo el cliente, info : " + e.getMessage() );
			assertEquals(cliUno.getApellido(), cliDos.getApellido());
		}	
		
	}

}
