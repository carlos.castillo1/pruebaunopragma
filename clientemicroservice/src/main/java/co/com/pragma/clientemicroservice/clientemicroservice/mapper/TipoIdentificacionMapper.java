package co.com.pragma.clientemicroservice.clientemicroservice.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import co.com.pragma.clientemicroservice.clientemicroservice.domain.TipoIdentificacion;
import co.com.pragma.clientemicroservice.clientemicroservice.dto.TipoIdentificacionDto;


@Mapper(componentModel = "spring")
public interface TipoIdentificacionMapper {
	
	
	public TipoIdentificacionDto tipoIdentificacionToTipoIdentificacionDTO(TipoIdentificacion tipoIdentificacion);
	public TipoIdentificacion tipoIdentificacionDTOToTipoIdentificacion(TipoIdentificacionDto tipoIdentificacionDTO);
	
	public List<TipoIdentificacionDto> listTipoIdentificacionToListTipoIdentificacionDTO(List<TipoIdentificacion> listTipoIdentificacion);
	public List<TipoIdentificacion> listTipoIdentificacionDTOToListTipoIdentificacion(List<TipoIdentificacionDto> listTipoIdentificacionDTO);
	
}
