package co.com.pragma.clientemicroservice.clientemicroservice.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import co.com.pragma.clientemicroservice.clientemicroservice.domain.TipoIdentificacion;
import co.com.pragma.clientemicroservice.clientemicroservice.dto.TipoIdentificacionDto;
import co.com.pragma.clientemicroservice.clientemicroservice.mapper.TipoIdentificacionMapper;
import co.com.pragma.clientemicroservice.clientemicroservice.repository.TipoIdentificacionRepository;



@Service
public class TipoIdentificacionServiceImpl implements TipoIdentificacionService{
	
	@Autowired
	private TipoIdentificacionRepository tipoIdentificacionRepository;
	
	@Autowired
	private TipoIdentificacionMapper tipoIdentificacionMapper;

	@Override
	public ResponseEntity<?> consultarTipoIdentificacion() throws Exception {
		List<TipoIdentificacion> listaTiIdent = tipoIdentificacionRepository.findAll();
		if(listaTiIdent.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK).body("Lista de tipos de identificacion vacia");
		}else {
			return ResponseEntity.status(HttpStatus.OK).body( this.tipoIdentificacionMapper.listTipoIdentificacionToListTipoIdentificacionDTO(listaTiIdent));
		}
		
	}

	@Override
	public ResponseEntity<?> consultarTipoIdentificacionPorId(Integer idTipoIdentificacion) throws Exception {
		Optional<TipoIdentificacion> tipoIdent = this.tipoIdentificacionRepository.findById(idTipoIdentificacion);
		if(tipoIdent.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No existe un tipo de identificacion con el id " + idTipoIdentificacion);
		}else {
			return ResponseEntity.status(HttpStatus.OK).body(this.tipoIdentificacionMapper.tipoIdentificacionToTipoIdentificacionDTO(tipoIdent.get()));
		}
	}

	@Override
	public ResponseEntity<?> guardarTipoIdentificacion(TipoIdentificacionDto nuevoTipoIdentificacion) throws Exception {
		try {
			if(nuevoTipoIdentificacion.getNombre().equals("") || nuevoTipoIdentificacion.getNombre().equals(null)){
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("debe ingresar un nombre para el tipo de identificacion");
			}
			TipoIdentificacionDto ti = this.tipoIdentificacionMapper.tipoIdentificacionToTipoIdentificacionDTO(this.tipoIdentificacionRepository.save( this.tipoIdentificacionMapper.tipoIdentificacionDTOToTipoIdentificacion(nuevoTipoIdentificacion)));		
			return ResponseEntity.status(HttpStatus.OK).body(ti);
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
	}

	@Override
	public ResponseEntity<?> eliminarTipoIdentificacionPorId(Integer idTipoIdentificacion) throws Exception {
		Optional<TipoIdentificacion> tipoIdent = this.tipoIdentificacionRepository.findById(idTipoIdentificacion);
		
		if(tipoIdent.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("no existe el tipo de identificacon con id " + idTipoIdentificacion);
		}else {
			this.tipoIdentificacionRepository.deleteById(idTipoIdentificacion);
			return ResponseEntity.status(HttpStatus.OK).body("TIPO DE IDENTIFICACION ELIMINADO");
		}		
	}

	@Override
	public ResponseEntity<?> actualizarTipoIdentificacion(TipoIdentificacionDto tipoIdentificacion) throws Exception {
		
		Optional<TipoIdentificacion> updateTipoIdentificacion = this.tipoIdentificacionRepository.findById(tipoIdentificacion.getId());
		
		if(updateTipoIdentificacion.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("NO hay tipos de identificacion con el id " + tipoIdentificacion.getId());
		}else {
			TipoIdentificacion ti = this.tipoIdentificacionRepository.save(this.tipoIdentificacionMapper.tipoIdentificacionDTOToTipoIdentificacion(tipoIdentificacion));
			return ResponseEntity.status(HttpStatus.OK).body(this.tipoIdentificacionMapper.tipoIdentificacionToTipoIdentificacionDTO(ti));
		}
		
	}

}
