package co.com.pragma.clientemicroservice.clientemicroservice.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "tipo_identificacion")
@Data 
@AllArgsConstructor 
@NoArgsConstructor
@Builder
public class TipoIdentificacion implements Serializable {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tiid", nullable = false)
	private Integer id;

	@Column(name = "nombre", nullable = false)
	@Size(min = 2, max = 20, message = "el nombre debe ser de 2 a 20 caracteres")
	private String nombre;
		
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idTipIdent")
	private List<Cliente> cliente = new ArrayList<>();
	
	
	
}