package co.com.pragma.clientemicroservice.clientemicroservice.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import co.com.pragma.clientemicroservice.clientemicroservice.domain.TipoIdentificacion;
import co.com.pragma.clientemicroservice.clientemicroservice.dto.TipoIdentificacionDto;



public interface TipoIdentificacionService {

	
	public ResponseEntity<?> consultarTipoIdentificacion() throws Exception;
	
	public ResponseEntity<?> consultarTipoIdentificacionPorId(Integer idTipoIdentificacion) throws Exception;
	
	public ResponseEntity<?> guardarTipoIdentificacion(TipoIdentificacionDto nuevoTipoIdentificacion)throws Exception;
	
	public ResponseEntity<?> eliminarTipoIdentificacionPorId(Integer idTipoIdentificacion)throws Exception;

	public ResponseEntity<?> actualizarTipoIdentificacion(TipoIdentificacionDto TipoIdentificacion)throws Exception;
}
