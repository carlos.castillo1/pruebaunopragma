package co.com.pragma.clientemicroservice.clientemicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.pragma.clientemicroservice.clientemicroservice.dto.TipoIdentificacionDto;
import co.com.pragma.clientemicroservice.clientemicroservice.mapper.TipoIdentificacionMapper;
import co.com.pragma.clientemicroservice.clientemicroservice.service.TipoIdentificacionService;


@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api/tipoIdentificacion")
public class TipoIdentificacionController {
	
	@Autowired
	private TipoIdentificacionService tipoIdentificacionService;
	
	@Autowired
	private TipoIdentificacionMapper tipoIdentificacionMapper;
	
	
	@GetMapping("/")
	public ResponseEntity<?> consultarTiposIdentificacion(){
		try {
			return tipoIdentificacionService.consultarTipoIdentificacion();
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@PostMapping("/")
	public ResponseEntity<?> guardarTipoIdentificacion( @RequestBody TipoIdentificacionDto tipoIdentificacion, BindingResult resultValidacion){
		try {
	
			return this.tipoIdentificacionService.guardarTipoIdentificacion(tipoIdentificacion);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	} 
	
	@PutMapping("/")	
	public ResponseEntity<?> actualizarTipoIdentificacion(@RequestBody TipoIdentificacionDto tipoIdentificacion){
		
		try {			
			return this.tipoIdentificacionService.actualizarTipoIdentificacion(tipoIdentificacion);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarTipoIdentificacion(@PathVariable("id") Integer id){
		try {			
			return this.tipoIdentificacionService.eliminarTipoIdentificacionPorId(id);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	

}