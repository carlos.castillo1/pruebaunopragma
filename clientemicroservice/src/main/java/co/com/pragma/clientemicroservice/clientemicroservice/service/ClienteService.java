package co.com.pragma.clientemicroservice.clientemicroservice.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import co.com.pragma.clientemicroservice.clientemicroservice.dto.ClienteDto;

public interface ClienteService {
	
	
	public ResponseEntity<?> consultarClientes() throws Exception;
	
	public ResponseEntity<?> consultarClientePorId(Integer idCliente) throws Exception;
	
	public ResponseEntity<?> guardarCliente(ClienteDto nuevoCliente) throws Exception;
	
	public ResponseEntity<?> eliminarClientePorId(Integer idCliente) throws Exception;

	public ResponseEntity<?> actualizarCliente(ClienteDto cliente) throws Exception;
	
	public ResponseEntity<?> consultarClientesEdadMayor(Integer edad) throws Exception;
	
	public ResponseEntity<?> consultarClientesEdadMenor(Integer edad) throws Exception;
	
	public ResponseEntity<?> consultarClientePorIdentificacion(Integer noIdentificacion) throws Exception;
	
}
