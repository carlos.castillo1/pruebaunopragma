package co.com.pragma.clientemicroservice.clientemicroservice.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor @Builder
public class ClienteDto {
	
	
	private Integer id;

	private String nombre;

	private String apellido;

	private Integer edad;

	private String ciudadNacimiento;

	private Integer noIdentificacion;
	
	private String foto;
	
	private Integer idTi;

}
