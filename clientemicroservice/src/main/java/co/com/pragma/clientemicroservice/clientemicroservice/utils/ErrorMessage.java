package co.com.pragma.clientemicroservice.clientemicroservice.utils;

import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor @Builder
public class ErrorMessage {
	private String cod;
	private List<Map<String, String>> messages;
}
