package co.com.pragma.clientemicroservice.clientemicroservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor @Builder
public class FotoDto {
	
	private String id;
	
	private String foto;
	
	private Integer idUsr;

}
