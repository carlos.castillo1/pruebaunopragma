package co.com.pragma.clientemicroservice.clientemicroservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import co.com.pragma.clientemicroservice.clientemicroservice.domain.Cliente;



public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

	public Cliente findByNoIdentificacion(Integer noIdentificacion);
	
	public List<Cliente> findByEdadGreaterThanEqual(Integer edad);
	
	public List<Cliente> findByEdadLessThanEqual(Integer edad);
}
