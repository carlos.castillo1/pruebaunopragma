package co.com.pragma.clientemicroservice.clientemicroservice.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.common.base.Function;

import co.com.pragma.clientemicroservice.clientemicroservice.cliente.FotoCliente;
import co.com.pragma.clientemicroservice.clientemicroservice.domain.Cliente;
import co.com.pragma.clientemicroservice.clientemicroservice.domain.TipoIdentificacion;
import co.com.pragma.clientemicroservice.clientemicroservice.dto.ClienteDto;
import co.com.pragma.clientemicroservice.clientemicroservice.dto.FotoDto;
import co.com.pragma.clientemicroservice.clientemicroservice.mapper.ClienteMapper;
import co.com.pragma.clientemicroservice.clientemicroservice.repository.ClienteRepository;
import co.com.pragma.clientemicroservice.clientemicroservice.repository.TipoIdentificacionRepository;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private ClienteMapper clienteMapper;
	
	@Autowired
	private TipoIdentificacionService tipoIdentificacionService;
	
	@Autowired
	private TipoIdentificacionRepository tipoIdentificacionRepository;
	
	@Autowired
	private FotoCliente fotoCliente;

	
	private String findFotoByIdUsr(List<FotoDto> fotos, Integer idUsr) {
		
		
		for(FotoDto f: fotos) {
			if(f.getIdUsr()!=null && f.getIdUsr().equals(idUsr)) {
				return f.getFoto();
			}	
		}
		return null;
	}
	
	@Override
	public ResponseEntity<?> consultarClientes() throws Exception {

		List<Cliente> listaClientes = clienteRepository.findAll();

		if (listaClientes.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK).body("NO HAY CLIENTES EN LA BASE DE DATOS");
		} else {
			List<ClienteDto> listaClientDto = this.clienteMapper.listClienteToListClienteDTO(listaClientes);
			List<FotoDto> listFotos = this.fotoCliente.consultarTfotos().getBody();
		
			
			for(ClienteDto cli: listaClientDto) {
				cli.setFoto(this.findFotoByIdUsr(listFotos, cli.getId()));
			}
			return ResponseEntity.ok().body(listaClientDto);
		}
	}

	@Override
	public ResponseEntity<?> consultarClientePorId(Integer idCliente) throws Exception {
		
		Optional<Cliente> cli = this.clienteRepository.findById(idCliente);
		if(cli.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("El cliente no existe en la base de datos");
		}else {
			ClienteDto cliAux = this.clienteMapper.ClienteToClienteDTO(cli.get());
			try {
				FotoDto foto = this.fotoCliente.consultarFotoPorIdUsr(cli.get().getId()).getBody();
				cliAux.setFoto(foto.getFoto());
				return ResponseEntity.ok().body(cliAux);
			}catch (Exception e) {
				return ResponseEntity.ok().body(cliAux);
			}

		}
	}

	@Override
	public ResponseEntity<?> guardarCliente(ClienteDto nuevoCliente) throws Exception {

		Cliente clientes = this.clienteRepository.findByNoIdentificacion(nuevoCliente.getNoIdentificacion());
		
		Optional<TipoIdentificacion> ti = this.tipoIdentificacionRepository.findById(nuevoCliente.getIdTi());
				
		if(ti.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("El tipo de identificacion no existe");
		}	

		if (clientes != null) {
			return ResponseEntity.status(HttpStatus.OK).body("ya existe un registro de este numero de identificacion en la base de datos");
		}

		Cliente clienteAux = this.clienteMapper.ClienteDTOToCliente(nuevoCliente);

		this.clienteRepository.save(clienteAux);
		
		if(nuevoCliente.getFoto() != null && !nuevoCliente.getFoto().equals("")) {
			this.fotoCliente.guardarFoto(FotoDto.builder().foto(nuevoCliente.getFoto()).idUsr(clienteAux.getId()).build());
		}
		
		ClienteDto clienteResponse = this.clienteMapper.ClienteToClienteDTO(clienteAux);
		clienteResponse.setFoto(nuevoCliente.getFoto());
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteResponse);
	}
	//añadir rollback
	@Override
	public ResponseEntity<?> eliminarClientePorId(Integer idCliente) throws Exception {

		Optional<Cliente> cli = this.clienteRepository.findById(idCliente);

		if (cli.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No se encuentran registros con el id " + idCliente);
		}
		
		FotoDto foto = this.fotoCliente.consultarFotoPorIdUsr(idCliente).getBody();
		
		if(foto != null) {
			this.fotoCliente.eliminarFoto(foto.getId());
		}
		this.clienteRepository.deleteById(idCliente);

		return ResponseEntity.status(HttpStatus.OK).body("CLIENTE ELIMINADO, ID: " + idCliente);
	}

	@Override
	public ResponseEntity<?> actualizarCliente(ClienteDto cliente) throws Exception {
		Optional<Cliente> cli = this.clienteRepository.findById(cliente.getId());
		Optional<TipoIdentificacion> ti = this.tipoIdentificacionRepository.findById(cliente.getIdTi());
		
		if(ti.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("El tipo de identificacion es incorrecto");
		}
		if (cli.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No se encuentran registros con el id " + cliente.getId());
		}
		if(cliente.getFoto() != null && !cliente.getFoto().equals("")) {
			try {
				FotoDto f = this.fotoCliente.consultarFotoPorIdUsr(cliente.getId()).getBody();
				f.setFoto(cliente.getFoto());
				this.fotoCliente.actualizarFotoPorId(f);
			}catch (Exception e) {
				FotoDto f = this.fotoCliente.consultarFotoPorIdUsr(cliente.getId()).getBody();
				f.setFoto(cliente.getFoto());
				this.fotoCliente.guardarFoto(f);
			}
		}
		Cliente nuevoCliente = this.clienteMapper.ClienteDTOToCliente(cliente);
		nuevoCliente.setIdTipIdent(ti.get());
		this.clienteRepository.save(nuevoCliente);
		
		return ResponseEntity.status(HttpStatus.CREATED).body("Cliente actualizado");

	}

	@Override
	public ResponseEntity<?> consultarClientesEdadMayor(Integer edad) throws Exception {
		
		List<Cliente> clienteFiltro = this.clienteRepository.findByEdadGreaterThanEqual(edad);
		
		if(clienteFiltro.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No hay clientes con edad mayor o igual a " + edad);
		}else {
			List<ClienteDto> listaClientDto = this.clienteMapper.listClienteToListClienteDTO(clienteFiltro);
			List<FotoDto> listFotos = this.fotoCliente.consultarTfotos().getBody();
			for(ClienteDto cli: listaClientDto) {
				cli.setFoto(this.findFotoByIdUsr(listFotos, cli.getId()));
			}
			return ResponseEntity.status(HttpStatus.OK).body(listaClientDto);
		}
	}

	@Override
	public ResponseEntity<?> consultarClientePorIdentificacion(Integer noIdentificacion) throws Exception {
		
		Cliente cli = this.clienteRepository.findByNoIdentificacion(noIdentificacion);
		
		if(cli == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("NO existe el cliente con no identificacion " + noIdentificacion);
		}else {
			ClienteDto clienteResponse = this.clienteMapper.ClienteToClienteDTO(cli);
			clienteResponse.setFoto(this.fotoCliente.consultarFotoPorIdUsr(cli.getId()).getBody().getFoto());
			return ResponseEntity.status(HttpStatus.OK).body(clienteResponse);
		}
	}

	@Override
	public ResponseEntity<?> consultarClientesEdadMenor(Integer edad) throws Exception {

		List<Cliente> menores = this.clienteRepository.findByEdadLessThanEqual(edad);
		
		if(menores.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("NO existe el cliente menores que " + edad);
		}else {
			List<ClienteDto> listaClientDto = this.clienteMapper.listClienteToListClienteDTO(menores);
			List<FotoDto> listFotos = this.fotoCliente.consultarTfotos().getBody();
			for(ClienteDto cli: listaClientDto) {
				cli.setFoto(this.findFotoByIdUsr(listFotos, cli.getId()));
			}
			return ResponseEntity.status(HttpStatus.OK).body(listaClientDto);
		}
	}
}
