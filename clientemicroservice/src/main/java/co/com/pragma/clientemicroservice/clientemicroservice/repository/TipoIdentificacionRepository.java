package co.com.pragma.clientemicroservice.clientemicroservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.clientemicroservice.clientemicroservice.domain.TipoIdentificacion;



public interface TipoIdentificacionRepository extends JpaRepository<TipoIdentificacion, Integer> {

}
