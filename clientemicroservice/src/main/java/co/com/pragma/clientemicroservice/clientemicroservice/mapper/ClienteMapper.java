package co.com.pragma.clientemicroservice.clientemicroservice.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import co.com.pragma.clientemicroservice.clientemicroservice.domain.Cliente;
import co.com.pragma.clientemicroservice.clientemicroservice.dto.ClienteDto;


@Mapper(componentModel = "spring")
public interface ClienteMapper {

	@Mapping(source = "idTipIdent.id", target = "idTi")
	public ClienteDto ClienteToClienteDTO(Cliente Cliente);
	
	
	@Mapping(source = "idTi", target = "idTipIdent.id")
	public Cliente ClienteDTOToCliente(ClienteDto ClienteDTO);
	
	public List<ClienteDto> listClienteToListClienteDTO(List<Cliente> listCliente);
	public List<Cliente> listClienteDTOToListCliente(List<ClienteDto> listClienteDTO);

	
}
