package co.com.pragma.clientemicroservice.clientemicroservice.cliente;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import co.com.pragma.clientemicroservice.clientemicroservice.dto.FotoDto;

@FeignClient(name = "fotoService", fallback = FotoHystrixFallBackFactory.class, path = "/api/foto")
public interface FotoCliente {
	/*
	 * path feign
	 */
	@GetMapping("/")
	public ResponseEntity<List<FotoDto>> consultarTfotos();
	
	@PostMapping("/")
	public ResponseEntity<FotoDto> guardarFoto(@RequestBody FotoDto foto);
	
	@GetMapping("/{id}")
	public ResponseEntity<FotoDto> consultarFotoPorId(@PathVariable("id") String id);
	
	@GetMapping("/cliente/{id}")
	public ResponseEntity<FotoDto> consultarFotoPorIdUsr(@PathVariable("id") Integer id);
	
	@PutMapping("/")
	public ResponseEntity<?> actualizarFotoPorId(@RequestBody FotoDto foto);
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarFoto(@PathVariable("id") String id);

}
