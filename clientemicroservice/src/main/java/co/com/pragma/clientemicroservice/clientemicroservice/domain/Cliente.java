package co.com.pragma.clientemicroservice.clientemicroservice.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "cliente" )
@Data @AllArgsConstructor @NoArgsConstructor
public class Cliente implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cliente", nullable = false)
	private Integer id;
	
	@Size(min = 3, max = 20, message = "El nombre debe estar entre 3 y 20 caracteres")
	@NotBlank(message = "Nombre vacio")
	private String nombre;
	
	@Size(min = 3, max = 20, message = "El apellido debe estar entre 3 y 20 caracteres")
	@NotBlank(message = "Apellido vacio")
	private String apellido;

	@Positive(message = "La edad debe ser positiva")
	@Min(value = 9, message = "La edad debe ser mayor de 8")
	@Max(value = 99, message = "la edad debe ser meno de 100")
	private Integer edad;
	
	@Column(name = "ciudad_nacimiento")
	@NotBlank(message = "Nombre de la ciudad vacio")
	@Size(min = 2, max = 20, message = "en mombre de la ciudad debe estar entre 2 y 20 caracteres")
	private String ciudadNacimiento;
	
	@Column(name = "no_identificacion", nullable = false, unique = true)
	@Positive(message = "EL numero de identificacion debe ser un valor positivo")
	private Integer noIdentificacion;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tiid", nullable = false)
	@JsonIgnoreProperties(value={ "hibernateLazyInitializer", "handler"},allowSetters = true)
	private TipoIdentificacion idTipIdent;
	
	
	

}
