package co.com.pragma.clientemicroservice.clientemicroservice.cliente;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import co.com.pragma.clientemicroservice.clientemicroservice.dto.FotoDto;

@Component
public class FotoHystrixFallBackFactory implements FotoCliente {
	
	@HystrixCommand
	@Override
	public ResponseEntity<List<FotoDto>> consultarTfotos() {
		List<FotoDto> l = new ArrayList<>();
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(l);
	}
	@HystrixCommand
	@Override
	public ResponseEntity<FotoDto> guardarFoto(FotoDto foto) {
		FotoDto f = new FotoDto();
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(f);
	}
	@HystrixCommand
	@Override
	public ResponseEntity<FotoDto> consultarFotoPorId(String id) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new FotoDto());
	}
	@HystrixCommand
	@Override
	public ResponseEntity<FotoDto> consultarFotoPorIdUsr(Integer id) {
		// TODO Auto-generated method stub
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new FotoDto());
	}
	@HystrixCommand
	@Override
	public ResponseEntity<?> actualizarFotoPorId(FotoDto foto) {
		// TODO Auto-generated method stub
		return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("NO se pudo actualizar la foto: servicio de fotos caido");
	}
	@Override
	public ResponseEntity<?> eliminarFoto(String id) {
		// TODO Auto-generated method stub
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("NO se pudo eliminar la foto: servicio de fotos caido");
	}

}
