package co.com.pragma.clientemicroservice.clientemicroservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class TipoIdentificacionDto {

	
	private Integer id;

	private String nombre;

}

