package co.com.pragma.clientemicroservice.clientemicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.pragma.clientemicroservice.clientemicroservice.dto.ClienteDto;
import co.com.pragma.clientemicroservice.clientemicroservice.service.ClienteService;


/*
 * clase controlador que expone los datos de los clientes
 */

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api/cliente")
public class ClienteRestController {
	
	@Autowired
	private ClienteService clienteService;


	@GetMapping("/")
	public ResponseEntity<?> consultarClientes(){
		
		try {
			return clienteService.consultarClientes();
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@GetMapping("/{id:[\\d]+}")
	public ResponseEntity<?> consultarClientePorId(@PathVariable("id") Integer id) {

		try {
			return clienteService.consultarClientePorId(id);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@PostMapping("/")
	public ResponseEntity<?> guardarCliente(@RequestBody  ClienteDto cliente){
		try {
			return this.clienteService.guardarCliente(cliente);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarCliente(@PathVariable("id") Integer id){
		try {
			
			return this.clienteService.eliminarClientePorId(id);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@PutMapping("/")
	public ResponseEntity<?> actualizarCliente(@RequestBody ClienteDto cliente){
		
		try {
			
			return this.clienteService.actualizarCliente(cliente);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@GetMapping("/consultarClientesEdadMayor/{edad}")
	public ResponseEntity<?> consultarClientesEdadMayor(@PathVariable("edad") Integer edad){
		try {
			return this.clienteService.consultarClientesEdadMayor(edad);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@GetMapping("/consultarClientesEdadMenor/{edad}")
	public ResponseEntity<?> consultarClientesEdadMenor(@PathVariable("edad") Integer edad){
		try {
			return this.clienteService.consultarClientesEdadMenor(edad);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@GetMapping("/consultarClientePorNoIdentificacion/{identificacion}")
	public ResponseEntity<?> consultarClientePorNoIdentificacion(@PathVariable("identificacion") Integer identificacion){
		try {
			return this.clienteService.consultarClientePorIdentificacion(identificacion);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}	
	}

}
